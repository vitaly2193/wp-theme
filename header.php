<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="container">
	<div class="row">

		<header class="site-header">

			<nav class="main-navigation">
				<?php
				wp_nav_menu( array(
					'theme_location' => 'nav-menu',
					'menu_id'        => 'primary-menu',
				) );
				?>
			</nav><!-- #site-navigation -->

			<div class="site-branding">
				<?php the_custom_logo(); ?>
				<div class="site-description">
					<?php bloginfo( 'name' ); ?> | <?php echo get_bloginfo( 'description', 'display' ); ?>
				</div>
			</div><!-- .site-branding -->

		</header><!-- #masthead -->
		
	</div>
</div>

