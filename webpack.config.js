const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

module.exports = {
    entry: ['./_dev/js/app.js', './_dev/css/app.scss'],
    output: {
        filename: './assets/js/bundle.js',
        path: path.resolve(__dirname)
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ['babel-preset-env']
                    }
                }
            },
            {
                test: /\.(sass|scss)$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
            },
            {
                test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
                exclude: [/images/],
                loader: 'file-loader',
                options: {
                    name: '[hash:8].[ext]',
                    outputPath: 'assets/fonts/',
                    publicPath: './../assets/fonts/'
                }
            },
            {
                test: /\.(jpe?g|png|gif|svg)(\?.*$|$)/,
                exclude: [/fonts/],
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    outputPath: 'assets/images/',
                    publicPath: './../assets/images/'
                }
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: './assets/css/main.min.css'
        })
    ],
    optimization: {
        minimizer: [
            new UglifyJSPlugin({
                cache: true,
                parallel: true
            }),
            new OptimizeCSSAssetsPlugin({})
        ]
    }
};