<?php

add_action( 'after_setup_theme', 'PROJECT_setup' );
function PROJECT_setup() {
    add_theme_support( 'title-tag' );

    add_theme_support( 'post-thumbnails' );

    register_nav_menus( array(
        'nav-menu' => esc_html__( 'Primary', 'PROJECT' ),
    ) );

    add_theme_support( 'html5',
        array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );

    add_theme_support( 'customize-selective-refresh-widgets' );
    add_theme_support( 'custom-logo',
        array(
            'height'      => 1000,
            'width'       => 1000,
            'flex-width'  => true,
            'flex-height' => true,
        ) );
}

add_action( 'widgets_init', 'PROJECT_widgets_init' );
function PROJECT_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'PROJECT' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'PROJECT' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}

add_action( 'wp_enqueue_scripts', 'PROJECT_scripts' );
function PROJECT_scripts() {
    wp_enqueue_style( 'PROJECT-style', get_stylesheet_uri() );
    wp_enqueue_style( 'PROJECT-main-styles', get_stylesheet_uri() . '/assets/css/main.min.css' );
    wp_enqueue_script( 'PROJECT-scripts', get_template_directory_uri() . '/assets/js/bundle.js', array(), '', true );
}

if ( function_exists( 'acf_add_options_page' ) ) {
    acf_add_options_page( array(
        'page_title' => 'Site options',
        'menu_title' => 'Site options',
        'menu_slug'  => 'site_options',
        'redirect'   => false,
    ) );
}